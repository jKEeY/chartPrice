import { Bar } from 'vue-chartjs'

export default {
  extends: Bar,
  props: ['data'],
  methods: {
    returnLabels() {
      let arr = [];

      for (let i = 0; i < this.data.length; i++) {
        arr.push(this.data[i].companyName);
      }
      return arr;
    },
    returnData() {
      let arr = [];
      for (let i = 0; i < this.data.length; i++) {
        arr.push(this.data[i].price);
      }
      console.log(arr);
      return arr;
    },
  },
  mounted () {
    // Переопределение базового рендер метода с реальными данными.
    this.renderChart({

      labels: this.returnLabels(),
      datasets: [{
        label: 'ai92-95#',
        backgroundColor: ['#ff9baa', '#66cdaa', '#a0d6b4'],
        data: this.returnData() //#
      }],
      
    })
  }
}

